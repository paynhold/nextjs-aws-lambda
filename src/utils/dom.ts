/**
 * @module consolidates our DOM access
 */

export namespace FocusClassNames {
  /** The class applied to an input if it has an error */
  export const errorInput = 'input-error';

  /** The id applied to the maincontent (for accessibility) */
  export const maincontent = 'main-content';
}

/**
 * Focuses the first element that matches the selector
 */
export function focusFirst(selector: string) {
  const element = document.querySelectorAll(selector);
  if (element.length) {
    (element[0] as HTMLElement).focus();
  }
}

/**
 * Focuses the last element that matches the selector
 */
export function focusLast(selector: string) {
  const element = document.querySelectorAll(selector);
  if (element.length) {
    (element[element.length - 1] as HTMLElement).focus();
  }
}

/**
 * Focuses the first ap input that has an error
 */
export const focusFirstErrorInput = () => focusFirst('.' + FocusClassNames.errorInput);

/**
 * Scroll to top in browser
 */
export function scrollToTop() {
  typeof window !== 'undefined' && window.scrollTo(0, 0);
}

/**
 * Returns the env that matches the url.
 * Example:
const env = getEnvFromHost({
  'prod': (host: string) => host.startsWith('prod'),
  'test': (host: string) => host.startsWith('test'),
  // Fallback
  'dev': () => true,
});
let config: {
  backend: string
};
if (env === 'prod') {
  config = {
    backend: 'prodBE'
  }
} else if (env === 'test') {
  config = {
    backend: 'testBE'
  }
} else {
  config = {
    backend: 'devBE'
  }
}
 */
export function getEnvFromHost<T extends { [key: string]: (str: string) => boolean }>(config: T): keyof T {
  const host =
    typeof window !== 'undefined' && typeof window.location !== 'undefined'
      ? window.location.host
      : '';
  const keys = Object.keys(config);
  for (const key of keys) {
    if (config[key](host)) return key;
  }
  throw new Error('Unknown environment');
}
