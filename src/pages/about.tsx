import * as React from 'react';
import { ImageBackgroundDiv, PageBody } from '../components';
import { rehydrate } from 'glamor';
import { setupPageStyles } from '../components/PageRoot';
import { fillFlexParent, flexCenterCenter } from '../styles/layout';
import Head from 'next/head';
import { Header } from '../components/Header';
import { Colors } from '../styles/defaults';

// Adds server generated styles to glamor cache.
// Has to run before any `style()` calls
// '__NEXT_DATA__.ids' is set in '_document.js'
if (typeof window !== 'undefined') {
  rehydrate(window['__NEXT_DATA__']['ids'])
}

export default () => {
  setupPageStyles();
  return (
    <div>
      <Head>
        <title>Pay n Hold</title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta charSet="utf-8"/>
      </Head>
      <PageBody header={<Header headerLink="/">header</Header>}>
        <section>
          <ImageBackgroundDiv imageSrc="/static/hero.jpg" width="100%" height="500px" backgroundSize="cover">
            <div className={`${fillFlexParent} ${flexCenterCenter}`}>
              <h1>We are bla bla</h1>
            </div>
          </ImageBackgroundDiv>
        </section>
        <section style={{textAlign: 'center'}}>
          <h2>About</h2>
          <p>To prevent scammers, etc.</p>
        </section>
      </PageBody>
    </div>
  )
};
