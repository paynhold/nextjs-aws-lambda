import * as React from 'react';
import { Button, Hero, PageBody, ImageBackgroundDiv, ResponsiveGrid, Card, ContentDiv } from '../components';
import { css, rehydrate } from 'glamor';
import { setupPageStyles } from '../components/PageRoot';
import Head from 'next/head';
import { Header } from '../components/Header';
import { Colors } from '../styles/defaults';
import { content, fillFlexParent, flex, flexColumn, flexRow } from '../styles/layout';

// Adds server generated styles to glamor cache.
// Has to run before any `style()` calls
// '__NEXT_DATA__.ids' is set in '_document.js'
if (typeof window !== 'undefined') {
  rehydrate(window['__NEXT_DATA__']['ids']);
}

export default () => {
  setupPageStyles();
  return (
    <div>
      <Head>
        <title>Pay n Hold</title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta charSet="utf-8"/>
      </Head>
      <PageBody header={<Header headerLink="/">header</Header>}>
        <section>
          <Hero
            imageSrc="/static/hero.jpg"
            heroTitle="Request a deposit securely and sell with confidence"
            heroCaption="Sell to genuine buyers. Feel safe when meeting up."
            element={<Button buttonSize="large" onClick={() => alert('clicked')}>Request deposit now</Button>}
          />
        </section>
        <section>
          <div className={`${css(flex, {padding: '1rem'})}`}>
            <div className={`${css({flex: '0 1 50%', padding: '2rem'})}`}>
              <h2>Easily request a deposit</h2>
              <p>
                Just create a request for a deposit and let us handle the payment safely.
              </p>
              <Button
                buttonColors={
                  {
                    backgroundColor: Colors.transparent,
                    borderColor: Colors.grey,
                    color: Colors.grey,
                    hoverBackgroundColor: Colors.grey,
                    hoverBorderColor: Colors.grey,
                  }
                }
              >Learn more</Button>
            </div>
            <div className={`${css({flex: '0 1 50%', padding: '2rem'})}`}>
              <ImageBackgroundDiv imageSrc="/static/connect.jpg" width="100%" height="100%" backgroundSize="cover"/>
            </div>
          </div>
        </section>
        <section>
          <ContentDiv backgroundcolor="#f7f9fa">
            <ResponsiveGrid>
              <Card
                imageSrc="/static/connect.jpg"
                heading="Sell safely to trusted buyers"
                description="Don't risk meeting a stranger when selling. Pay 'n' Hold knows who you are dealing with. Feel safe when meeting up to complete your transaction."/>
              <Card
                imageSrc="/static/connect.jpg"
                heading="Filter out time wasters"
                description="Filter out buyers who promise to buy that don't show up. Only deal with genuine buyers with Pay 'n' Hold."/>
              <Card
                imageSrc="/static/connect.jpg"
                heading="Low fees"
                description="Free to send requests and only $2 if the sale transaction is successful. Your safety is worth more."/>
            </ResponsiveGrid>
          </ContentDiv>
        </section>
        <section>
          <ContentDiv backgroundcolor="#23292F" style={{color: Colors.white, textAlign: 'center'}}>
            <h2>Start taking deposits online for your business now.</h2>
            <p>Easily send a request for payment.... bla</p>
          </ContentDiv>
        </section>
        <section>
          <ContentDiv backgroundcolor="#f7f9fa">
            <ResponsiveGrid>
              This is where we put all our customers. Maybe like United Currency Exchange, etc.
            </ResponsiveGrid>
          </ContentDiv>
        </section>
      </PageBody>
    </div>
  )
};
