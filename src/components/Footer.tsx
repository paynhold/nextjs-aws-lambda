import * as React from 'react';
import { css } from 'glamor';
import { content, flex, flexCenter, flexColumn } from '../styles/layout';
import { resetUl } from '../styles/ul';


let footerClass = css(content, {padding: '30px'});
let footerContainerClass = css(flex, flexColumn, flexCenter, {width: '100%', padding: '1rem'});

export class Footer extends React.Component {
  render() {
    return (
      <footer data-comment="Footer" className={`${footerClass}`}>
        <div data-comment="FooterContent" className={`${footerContainerClass}`}>
          <ul className={`${css(resetUl, {margin: '0 auto', fontSize:'.9rem', '>*': {marginRight: '18px !important'}, '>*:last-child': {marginRight: '0 !important'}})}`}>
            <li className={`${css({display: 'inline'})}`}>About</li>
            <li className={`${css({display: 'inline'})}`}>Support</li>
            <li className={`${css({display: 'inline'})}`}>Security</li>
            <li className={`${css({display: 'inline'})}`}>Contact</li>
          </ul>
          <ul className={`${css(resetUl, {margin: '0 auto', fontSize:'.9rem', '>*': {marginRight: '18px !important'}, '>*:last-child': {marginRight: '0 !important'}})}`}>
            <li className={`${css({display: 'inline'})}`}>Legal</li>
            <li className={`${css({display: 'inline'})}`}>Privacy</li>
          </ul>
          <ul className={`${css(resetUl, {margin: '0 auto', fontSize:'.9rem', '>*': {marginRight: '18px !important'}, '>*:last-child': {marginRight: '0 !important'}})}`}>
            <li className={`${css({display: 'inline'})}`}>Facebook</li>
            <li className={`${css({display: 'inline'})}`}>Twitter</li>
          </ul>
        </div>
      </footer>
    );
  }
}