import * as React from 'react';
import { PageRoot } from './PageRoot';
import { Footer } from './Footer';
import { fillFlexParent } from '../styles/layout';
import { css } from 'glamor';
import { Fonts } from '../styles/Fonts';

export interface PageBodyProps {
  header: React.ReactNode
}

const contentClass = css(fillFlexParent, {paddingTop: '60px'});

export class PageBody extends React.Component<PageBodyProps, {}> {
  componentDidMount() {
    Fonts();
  }

  render() {
    const {header, children} = this.props;
    return (
      <PageRoot>
        {header}
        <div className={`${contentClass}`}>
          {children}
        </div>
        <Footer/>
      </PageRoot>
    );
  }
}