import * as React from 'react';
import { SmallVerticalSpace } from './Spacing';
import { css } from 'glamor';
import { Colors } from '../styles/defaults';
import { NavState } from './Header';

export namespace HamburgerMenuStyles {
  export const root = css({
    width: '60px',
    height: '60px',
    border: 'none',
    outline: 'none',
    backgroundColor: Colors.transparent,
    cursor: 'pointer',
    display: 'inline-block',
    padding: '6px',
    textAlign: ['center', '-webkit-center'],
    $nest: {
      '&:focus': {
        outline: 'thin dotted',
        outlineColor: Colors.warmGrey4
      },
      '&:active': {
        outline: 'thin dotted',
        outlineColor: Colors.warmGrey4
      }
    },
    '@media(min-width: 480px)': {
      display: 'none'
    }
  });

  export const line = css({
    width: '22px',
    height: '3px',
    borderRadius: '100px',
    backgroundColor: Colors.projectName
  });
}

export interface HamburgerMenuProps {
  navState: NavState
}

/** Toggles the global sidebar if any */
export const HamburgerMenu: React.StatelessComponent<HamburgerMenuProps> = ({navState}) => {
  return (
    <button className={`${HamburgerMenuStyles.root}`} onClick={() => navState.toggle()}>
      <div className={`${HamburgerMenuStyles.line}`}/>
      <SmallVerticalSpace space={5}/>
      <div className={`${HamburgerMenuStyles.line}`}/>
      <SmallVerticalSpace space={5}/>
      <div className={`${HamburgerMenuStyles.line}`}/>
    </button>
  );
}