import * as React from 'react';
import { content, flex, flexBetweenJustified, flexCenter, flexRow } from '../styles/layout';
import { compose, css } from 'glamor';
import glamorous from 'glamorous';
import { MenuItem } from './MenuItem';
import { HamburgerMenu } from './HamburgerMenu';
import { Colors } from '../styles/defaults';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';

export interface HeaderProps {
  headerLink?: string,
}

let headerClass = css(content, flex, flexCenter, {
  position: 'fixed',
  backgroundColor: Colors.bodyBackground,
  width: '100%'
});
let headerContainerClass = css(flex, flexRow, flexBetweenJustified, {width: '100%', height: '60px'});
let logoContainerClass = compose(flex, flexRow, flexCenter, {textDecoration: 'none', padding: '22px'});
let navClass = compose(flexCenter, flexCenter,
  {
    '@media(max-width: 480px)': {
      height: '0',
      position: 'fixed',
      left: '0',
      top: '60px',
      right: '0',
      overflowY: 'hidden',
      backgroundColor: Colors.bodyBackground,
      WebkitTransitionProperty: 'all',
      WebkitTransitionDuration: '0.5s',
      WebkitTransitionTimingFunction: 'cubic-bezier(0, 1, 0.5, 1)',
      MozkitTransitionProperty: 'all',
      MozkitTransitionDuration: '0.5s',
      MozkitTransitionTimingFunction: 'cubic-bezier(0, 1, 0.5, 1)',
      transitionProperty: 'all',
      transitionDuration: '0.5s',
      transitionTimingFunction: 'cubic-bezier(0, 1, 0.5, 1)'

    },
    '@media(min-width: 480px)': {
      display: ['flex'],
      height: '60px'
    }
  }
);

const showNavClass = css({'@media(max-width: 480px)': {height: '180px'}});
const hideNavClass = css({'@media(max-width: 480px)': {height: '0'}});

const HeaderContainer = glamorous.div<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> & { height?: number }>(
  content,
  ({height}) => ({
    height: `${height}px`
  })
);

export const Logo = ({height = 40}) => {
  return <strong>Pay n Hold</strong>;
};

export class NavState {
  @observable isShowing = false;
  @action show = () => {
    this.isShowing = true;
  }
  @action hide = () => this.isShowing = false;
  @action toggle = () => {
    this.isShowing = !this.isShowing;
  };
}

const navState = new NavState();

@observer
export class Header extends React.Component<HeaderProps, {}> {
  render() {
    const headerHeight = 60;
    const headerLinkProp = this.props.headerLink ? {href: this.props.headerLink} : {};
    return (
      <HeaderContainer data-comment="Header" className={`${headerClass}`} height={headerHeight}>
        <div data-comment="HeaderContent" className={`${headerContainerClass}`}>
          <a {...headerLinkProp} className={`${logoContainerClass}`}>
            <div style={{display: 'inline-block'}}>
              <Logo/>
            </div>
          </a>
          <nav className={`${navClass} ${navState.isShowing ? showNavClass : hideNavClass }`} role="navigation">
            <MenuItem label="HOW IT WORKS" href="how-it-works"/>
            <MenuItem label="BUSINESS" href="business"/>
          </nav>
          <HamburgerMenu navState={navState}/>
        </div>
      </HeaderContainer>
    );
  }
}