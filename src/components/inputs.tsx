import * as React from 'react';
import { css, Rule } from 'glamor';
import { Colors, DefaultStyles, FontFamilies, FontSizes } from '../styles/defaults';

export enum InputType {
  email,
  number,
  password,
  text,
  tel,
  url,
}

export interface InputProps {
  id?: string,
  disabled?: boolean,
  name?: string,
  type?: InputType,
  placeholder?: string,
  minLength?: number,
  maxLength?: number,
  autoFocus?: boolean,
  className?: Rule,

  value?: string,
  onChange?: (value: string) => any,

  error?: Rule;

  onFocus?: () => any,
  onBlur?: () => any,
  onKeyDown?: (e: React.SyntheticEvent<any>) => any;
}

export namespace InputStyles {
  import borderRadius = DefaultStyles.borderRadius;
  export const color = Colors.inputTextColor;
  export const borderColorNormal = '#dcdcdc';
  export const borderColorOnFocus = Colors.primary;
  export const errorColor = Colors.red;

  const root = css(
    {
      display: 'block',
      width: '100%',
      height: 40,
      padding: '.5rem .75rem',
      lineHeight: 'normal',
      color: color,
      border: `1px solid ${borderColorNormal}`,
      background: '#fff',
      borderRadius: DefaultStyles.borderRadius,
      outline: 'none',
      transition: 'color .2s',
      fontFamily: FontFamilies.text,
      fontSize: FontSizes.input,
    }
  );

  export const rootClass = css(root, {
    ':hover': {
      borderColor: borderColorOnFocus,
    },
    ':focus': {
      borderColor: borderColorOnFocus,
      padding: '6px 9px'
    },
    ':disabled': {
      cursor: 'not-allowed',
      background: Colors.bodyBackground,
    }
  });
}

export class Input extends React.Component<InputProps, {}> {
  refs: {
    [key: string]: any;
    input: HTMLInputElement;
  }

  render() {
    const style = this.props.error
      ? {
        color: InputStyles.errorColor,
        borderColor: InputStyles.errorColor
      }
      : {};

    return <input
      id={this.props.id}
      disabled={this.props.disabled}
      name={this.props.name}
      className={`${css(
        InputStyles.rootClass,
      )}`}
      style={style}
      type={InputType[this.props.type || InputType.text]}
      placeholder={this.props.placeholder}
      value={this.props.value || ''}
      minLength={this.props.minLength}
      maxLength={this.props.maxLength}
      onChange={this.handleChange}
      onFocus={this.props.onFocus}
      onBlur={this.props.onBlur}
      onKeyDown={this.props.onKeyDown}
      autoFocus={this.props.autoFocus}
    />;
  }

  handleChange = (evt: React.FormEvent<HTMLInputElement>) => {
    let value = (evt.target as HTMLInputElement).value;
    this.props.onChange(value);
  }
}