import { Breakpoints, Spacing } from '../styles/defaults';

export let defaultValues = {
  verticalSpacing: Spacing.verticalBetweenFields,
  horizontalSpacing: Spacing.horizontalBetweenFields,

  breakpoints: {
    phone: Breakpoints.sm,
  }
};

export const SmallVerticalSpace = (props: { space?: number }) => {
  return <div style={{ height: props.space || defaultValues.verticalSpacing }}></div>;
};

export const SmallHorizontalSpace = (props: { space?: number }) => {
  return <div style={{ width: props.space || defaultValues.horizontalSpacing, display: 'inline-block' }}></div>;
};