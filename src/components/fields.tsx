import * as React from 'react';
import { Label } from './labels';
import { css } from 'glamor';
import { Colors, FontFamilies, FontSizes } from '../styles/defaults';
import errorInputClass = FieldStyles.errorInputClass;

export namespace FieldStyles {
  export const errorColor = Colors.error;

  export const rootClass = css({
      display: 'block',
      marginBottom: '1rem'
    }
  );

  export const errorTextClass = css({
    marginTop: '.25rem',
    font: FontFamilies.text,
    color: errorColor,
  });

  export const errorInputClass = css({
    '& input': {
      borderColor: errorColor,
    }
  })

  export const fieldHelpClass = css({
    display: 'block',
    fontSize: FontSizes.smallText,
    marginTop: '.25rem'
  })
}

export const FieldErrorText = ({message, id}: { message: string, id?: string }) => {
  return <div id={id ? id + '-error' : ''} className={`${FieldStyles.errorTextClass}`} aria-live="assertive"
              aria-atomic="true">
    {message}
  </div>;
}

export interface FieldProps {
  id?: string;
  error?: string;
  label?: string;
  help?: string;
}

export class Field extends React.Component<FieldProps, {}> {
  render() {
    return (
      <div className={`${css(FieldStyles.rootClass, this.props.error && FieldStyles.errorInputClass)}`}>
        <Label htmlFor={this.props.id} error={this.props.error} children={this.props.label}/>
        {this.props.children}
        {
          this.props.error && <FieldErrorText id={this.props.id} message={this.props.error}/>
        }
        <small className={`${FieldStyles.fieldHelpClass}`}>{this.props.help}</small>
      </div>
    );
  }
}

