import * as React from 'react';
import { css } from 'glamor';
import glamorous from 'glamorous';
import { Colors } from '../styles/defaults';

export interface ButtonProps {
  buttonSize?: string,
  loading?: boolean,
  buttonColors?: {
    backgroundColor: string,
    borderColor: string,
    color: string,
    hoverBackgroundColor: string,
    hoverBorderColor: string,
  }
}

export interface ButtonColors {
}

export let primaryButton = {
  backgroundColor: Colors.primary,
  borderColor: Colors.primary,
  color: Colors.white,
  ':active,:hover': {
    backgroundColor: Colors.primaryHover,
    borderColor: '#01549b'
  }
};

let buttonLarge = css({
  padding: '.75rem 1.5rem',
  fontSize: '1.25rem',
});

let buttonClass = css({
  display: 'inline-block',
  borderRadius: '.3rem',
  textAlign: 'center',
  whiteSpace: 'nowrap',
  verticalAlign: 'middle',
  userSelect: 'none',
  border: '2px solid',
  outline: 'none',
  WebkitTransition: 'all .2s ease-in-out',
  OTransition: 'all .2s ease-in-out',
  transition: 'all .2s ease-in-out',
  padding: '.5rem 1rem',
  fontSize: '1rem',
  cursor: 'pointer',
  ':focus,:active': {
    outline: 'none'
  }
});

export const Button = glamorous.button<ButtonProps>(
  buttonClass,
  ({buttonSize, buttonColors}) => css(
    buttonSize === 'large' ? buttonLarge : {},
    buttonColors ? {
      backgroundColor: buttonColors.backgroundColor,
      borderColor: buttonColors.color,
      color: buttonColors.color,
      ':active,:hover': {
        backgroundColor: buttonColors.color,
        color: Colors.white,
      }
    } : primaryButton
  )
);