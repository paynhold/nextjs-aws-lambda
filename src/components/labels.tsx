import * as React from 'react'
import { Colors } from '../styles/defaults';
import { css } from 'glamor';

export namespace LabelStyles {
  export const errorColor = Colors.error;

  export const rootClass = css(
    {
      fontSize: '.875rem',
      fontWeight: 'bold',
      display: 'inline-block',
      marginBottom: '.5rem',
      color: '#4d4d4d',
      transition: 'color .2s',
    }
  );
  export const errorClass = css({
    color: errorColor
  });
}

export interface LabelProps {
  id?: string;
  htmlFor?: string;
  error?: string;
}

export class Label extends React.Component<LabelProps, {}> {
  render() {
    return (
      <label
        id={this.props.id}
        className={`${css(LabelStyles.rootClass, this.props.error && LabelStyles.errorClass)}`}
        htmlFor={this.props.htmlFor}>{this.props.children}</label>
    );
  }
}