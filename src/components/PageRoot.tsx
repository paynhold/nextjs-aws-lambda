import * as React from 'react';
import { FocusClassNames, focusFirst } from '../utils/dom';
import { Colors } from '../styles/defaults';
import { css } from 'glamor';
import glamorous from 'glamorous';
import { flex, flexColumn, fullScreen } from '../styles/layout';

export function setupPageStyles() {
  css.insert('/*! normalize.css v7.0.0 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,footer,header,nav,section{display:block}h1{font-size:2em;margin:.67em 0}figcaption,figure,main{display:block}figure{margin:1em 40px}hr{box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:inherit}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}audio,video{display:inline-block}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:sans-serif;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=reset],[type=submit],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:.35em .75em .625em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{display:inline-block;vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details,menu{display:block}summary{display:list-item}canvas{display:inline-block}template{display:none}[hidden]{display:none}/*# sourceMappingURL=normalize.min.css.map */');
  // 'Lato':300, 700 style generated using typography.js
  css.insert('html{font:112.5%/1.61em \'Lato\',sans-serif;box-sizing:border-box;overflow-y:scroll;}*{box-sizing:inherit;}*:before{box-sizing:inherit;}*:after{box-sizing:inherit;}body{color:hsla(0,0%,0%,0.8);font-family:\'Lato\',sans-serif;font-weight:300;word-wrap:break-word;font-kerning:normal;-moz-font-feature-settings:"kern", "liga", "clig", "calt";-ms-font-feature-settings:"kern", "liga", "clig", "calt";-webkit-font-feature-settings:"kern", "liga", "clig", "calt";font-feature-settings:"kern", "liga", "clig", "calt";}img{max-width:100%;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}h1{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;color:inherit;font-family:\'Lato\',sans-serif;font-weight:700;text-rendering:optimizeLegibility;font-size:1.618rem;line-height:1.1;}h2{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;color:inherit;font-family:\'Lato\',sans-serif;font-weight:700;text-rendering:optimizeLegibility;font-size:1.33471rem;line-height:1.1;}h3{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;color:inherit;font-family:\'Lato\',sans-serif;font-weight:700;text-rendering:optimizeLegibility;font-size:1.21225rem;line-height:1.1;}h4{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;color:inherit;font-family:\'Lato\',sans-serif;font-weight:700;text-rendering:optimizeLegibility;font-size:1rem;line-height:1.1;}h5{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;color:inherit;font-family:\'Lato\',sans-serif;font-weight:700;text-rendering:optimizeLegibility;font-size:0.90825rem;line-height:1.1;}h6{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;color:inherit;font-family:\'Lato\',sans-serif;font-weight:700;text-rendering:optimizeLegibility;font-size:0.86558rem;line-height:1.1;}hgroup{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}ul{margin-left:1.61rem;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;list-style-position:outside;list-style-image:none;}ol{margin-left:1.61rem;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;list-style-position:outside;list-style-image:none;}dl{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}dd{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}p{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}figure{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}pre{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;font-size:0.85rem;line-height:1.61rem;}table{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;font-size:1rem;line-height:1.61rem;border-collapse:collapse;width:100%;}fieldset{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}blockquote{margin-left:1.61rem;margin-right:1.61rem;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}form{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}noscript{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}iframe{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}hr{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:calc(1.61rem - 1px);background:hsla(0,0%,0%,0.2);border:none;height:1px;}address{margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;margin-bottom:1.61rem;}b{font-weight:700;}strong{font-weight:700;}dt{font-weight:700;}th{font-weight:700;}li{margin-bottom:calc(1.61rem / 2);}ol li{padding-left:0;}ul li{padding-left:0;}li > ol{margin-left:1.61rem;margin-bottom:calc(1.61rem / 2);margin-top:calc(1.61rem / 2);}li > ul{margin-left:1.61rem;margin-bottom:calc(1.61rem / 2);margin-top:calc(1.61rem / 2);}blockquote *:last-child{margin-bottom:0;}li *:last-child{margin-bottom:0;}p *:last-child{margin-bottom:0;}li > p{margin-bottom:calc(1.61rem / 2);}code{font-size:0.85rem;line-height:1.61rem;}kbd{font-size:0.85rem;line-height:1.61rem;}samp{font-size:0.85rem;line-height:1.61rem;}abbr{border-bottom:1px dotted hsla(0,0%,0%,0.5);cursor:help;}acronym{border-bottom:1px dotted hsla(0,0%,0%,0.5);cursor:help;}abbr[title]{border-bottom:1px dotted hsla(0,0%,0%,0.5);cursor:help;text-decoration:none;}thead{text-align:left;}td,th{text-align:left;border-bottom:1px solid hsla(0,0%,0%,0.12);font-feature-settings:"tnum";-moz-font-feature-settings:"tnum";-ms-font-feature-settings:"tnum";-webkit-font-feature-settings:"tnum";padding-left:1.07333rem;padding-right:1.07333rem;padding-top:0.805rem;padding-bottom:calc(0.805rem - 1px);}th:first-child,td:first-child{padding-left:0;}th:last-child,td:last-child{padding-right:0;}');
  css.global('html,body', {height: '100%', width: '100%', padding: 0, margin: 0});
  css.global('html', {MozBoxSizing: 'border-box', WebkitBoxSizing: 'border-box', boxSizing: 'border-box'});
  css.global('body', {lineHeight: 1.5});
  css.global('*,*:before,*:after', {boxSizing: 'inherit'});
  css.global('body', {backgroundColor: Colors.white});
}

const AccessibilityLink = glamorous.a(
  {
    position: 'absolute',
    left: '-1000px',
    top: '-1000px',
    width: '1px',
    height: '1px',
    textAlign: 'left',
    overflow: 'hidden',
    ':focus,:active,:hover': {
      position: 'absolute',
      left: 0,
      top: 0,
      width: 'auto',
      height: 'auto',
      overflow: 'visible',
      backgroundColor: '#ff3',
      border: '1px dotted #000'
    }
  }
);

const pageRootClass = css(flex, flexColumn, fullScreen);

/**
 * Creates a full page height flexbox for us to work with
 */
export class PageRoot extends React.Component<{}, {}> {
  render(): any {
    return ([
      <AccessibilityLink
        key="skipContentLink"
        href={`#${FocusClassNames.maincontent}`}
        onClick={(e) => {
          e.preventDefault();
          focusFirst(`#${FocusClassNames.maincontent}`);
        }}>Skip to content</AccessibilityLink>,
      <div
        key="mainContent"
        data-comment="HeaderBodyCenteredPage"
        className={`${pageRootClass}`}
        children={this.props.children}/>
    ]);
  }
}