import * as React from 'react';
import { css } from 'glamor';
import { flex, flexBetweenJustified, flexWrap } from '../styles/layout';

const gridClass = css(flex, flexWrap,
  {
    padding: '1rem',
    '@media(max-width: 480px)': {
      flexDirection: 'column'
    }
  }
);

/**
 * Example usage
 *
 <ResponsiveGrid>
   <Card imageSrc="/static/connect.jpg" heading="What?" description="This is a service"/>
   <Card imageSrc="/static/connect.jpg" heading="What?" description="This is a service"/>
   <Card imageSrc="/static/connect.jpg" heading="What?" description="This is a service"/>
   <Card imageSrc="/static/connect.jpg" heading="What?" description="This is a service"/>
   <Card imageSrc="/static/connect.jpg" heading="What?" description="This is a service"/>
 </ResponsiveGrid>
 */
export class ResponsiveGrid extends React.Component {
  render() {
    const {children} = this.props;
    return (
      <div className={`${gridClass}`}>
        {children}
      </div>
    )
  }
}