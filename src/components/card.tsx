import * as React from 'react';
import { css } from 'glamor';
import { flex } from '../styles/layout';

export interface CardProps {
  imageSrc?: string;
  heading?: string;
  description?: string;
}

const cardClass = css({flex: '0 1 calc(33.3% - 0.6rem)', margin: '0 .25rem'});
const imageClass = css({width: '100%', height: 'auto', border: 'none'});
const contentClass = css({padding: '0.5rem', textAlign: 'center'})

export class Card extends React.Component<CardProps, {}> {
  render() {
    const {imageSrc, heading, description} = this.props;
    return (
      <article className={`${cardClass}`}>
        <picture>
          <img src={imageSrc} className={`${imageClass}`}/>
        </picture>
        <div className={`${contentClass}`}>
          {heading ? <h2>{heading}</h2> : null}
          {description ? <p>{description}</p> : null}
        </div>
      </article>
    );
  }
}