import * as React from 'react';
import { ImageBackgroundDiv } from './ImageBackgroundDiv';
import { fillFlexParent, flexCenterCenter, flexColumn } from '../styles/layout';
import { css } from 'glamor';
import { Colors } from '../styles/defaults';

export interface HeroProps {
  imageSrc: string;
  heroTitle?: string;
  heroCaption?: string;
  element?: React.ReactNode;
}

const heroContentClass = css(fillFlexParent, flexCenterCenter, flexColumn, {textAlign: 'center', padding: '20px'});
const heroTitleClass = css(
  {
    '@media(max-width: 480px)': {
      fontSize: '1.8rem'
    },
    color: Colors.white,
    fontSize: '2rem',
    fontWeight: 300
  }
);
const heroCaptionClass = css(
  {
    '@media(max-width: 480px)': {
      fontSize: '1.2rem'
    },
    color: Colors.white,
    fontWeight: 200
  }
);

export class Hero extends React.Component<HeroProps, {}> {
  render() {
    const {imageSrc, heroTitle, heroCaption, element} = this.props;
    return (
      <ImageBackgroundDiv
        imageSrc={imageSrc}
        gradient
        width="100%"
        height="500px"
        backgroundSize="cover"
      >
        <div className={`${heroContentClass}`}>
          <h1 className={`${heroTitleClass}`}>{heroTitle}</h1>
          <h2 className={`${heroCaptionClass}`}>{heroCaption}</h2>
          {element}
        </div>
      </ImageBackgroundDiv>
    );
  }
}