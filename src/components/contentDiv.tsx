import * as React from 'react';
import { css } from 'glamor';
import { content } from '../styles/layout';

export interface ContentDivProps extends React.HTMLProps<HTMLDivElement> {
  backgroundcolor: string;
}
const contentDivClass = css(content, {margin: '0 auto', padding: '30px'});

export class ContentDiv extends React.Component<ContentDivProps, {}> {
  render() {
    const {backgroundcolor, children} = this.props;
    return (
      <div className={`${css(contentDivClass, {backgroundColor: backgroundcolor})}`} {...this.props}>
        {children}
      </div>
    );
  }
}