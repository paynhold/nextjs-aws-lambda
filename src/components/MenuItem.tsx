import * as React from 'react';
import { Colors } from '../styles/defaults';
import { css } from 'glamor';

export interface MenuItemProps {
  label: string,
  href: string,
}

let menuItemClass = css({
  display: 'block',
  lineHeight: '60px',
  height: '60px',
  flexShrink: 0,
  paddingLeft: '22px',
  paddingRight: '22px',
  verticalAlign: 'center',
  textDecoration: 'none',
  transition: 'all 0.3s linear 0s',
  fontWeight: 300,
  fontSize: '.8rem',
  color: Colors.anchorColor,
  '.active,:hover,:focus': {
    color: Colors.anchorFocusColor,
    borderBottomColor: Colors.red,
    borderBottomWidth: '4px',
    borderBottomStyle: 'solid'
  }
});

export class MenuItem extends React.Component<MenuItemProps, {}> {
  render() {
    const {label, href} = this.props;
    return (
      <a href={href} className={`${menuItemClass}`}>{label}</a>
    );
  }
}