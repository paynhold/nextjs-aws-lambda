import * as React from 'react';
import glamorous from 'glamorous';
import { flex } from '../styles/layout';

export interface BackgroundImageProps {
  imageSrc: string,
  width: string,
  height: string,
  backgroundSize: string,
  gradient?: boolean,
}

export const ImageBackgroundDiv = glamorous.div<BackgroundImageProps>(
  flex,
  ({imageSrc, width, height, backgroundSize, gradient}) => ({
    backgroundImage: `${gradient ? 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),' : ''} url(${imageSrc})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    width: width,
    height: height,
    WebkitBackgroundSize: backgroundSize,
    MozBackgroundSize: backgroundSize,
    backgroundSize: backgroundSize,
  })
);