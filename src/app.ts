import * as express from 'express';
import * as next from 'next';

const dev = process.env.NODE_ENV !== 'production';
const dir = process.env.NODE_ENV !== 'production' ? './dist' : '.';
const port = parseInt(process.env.PORT, 10) || 3008;
const app = next({dir, dev});
const handle = app.getRequestHandler();

function createServer() {
  const server = express();
  server.get('*', (req, res) => {
    return handle(req, res);
  });
  return server;
}

if (process.env.IN_LAMBDA) {
  module.exports = createServer();
} else {
  app.prepare()
    .then(() => {
      const server = createServer();
      server.listen(port, (err) => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`);
      });
    })
    .catch((ex) => {
      console.error(ex.stack);
      process.exit(1);
    });
}