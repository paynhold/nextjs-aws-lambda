// Colors : https://app.frontify.com/d/zYF6HCW0aIp5/ui-library#/overview/global-colour-palette
export namespace Colors {
  export const transparent = 'transparent';

  export const red = '#DC1928';
  export const blue = '#0275d8';
  export const grey = '#382F2D';
  export const white = '#fff';

  export const primary = '#0275d8';
  export const primaryHover = '#025aa5';

  export const footer = '#23292F';

  // Notification colors
  export const success = '#067A3D';
  export const successBackground = '#DFF0D8';
  export const successBorder = '#D1E8CE';
  export const error = '#d9534f';
  export const errorBackground = '#F2DEDE';
  export const errorBorder = '#EDCBCB';
  export const warning = '#8A6D3B';
  export const warningBackground = '#FCF8E2';
  export const warningBorder = '#F3EDD5';
  export const info = '#0D72A8';
  export const infoBackground = '#E6EFF5';
  export const infoBorder = '#D3E2EB';

  export const warmGrey1 = '#F9F8F7';
  export const warmGrey2 = '#F3F1EE';
  export const warmGrey3 = '#E2DFDA';
  export const warmGrey4 = '#807370';
  export const warmGrey5 = '#5E514D';

  export const coolGrey1 = '#F3F4F5';
  export const coolGrey2 = '#D0D5D8';
  export const coolGrey3 = '#66727A';
  export const coolGrey4 = '#535F67';
  export const coolGrey5 = '#2D3238';

  // Semantic names for common colors
  export let textColor = warmGrey4;
  export let headingColor = grey;
  export let projectName = coolGrey4;
  export let bodyBackground = white;

  export let buttonPrimaryBg = primary;
  export let buttonSecondaryFg = white;
  export let buttonPrimaryHoverBg = primaryHover;
  export let buttonActiveBg = primary;
  export let buttonDisabledBg = warmGrey4;

  export let labelColor = warmGrey4;
  export let inputTextColor = grey;
  export let inputStarColor = red;

  export let anchorColor = warmGrey4;
  export let anchorFocusColor = red;

  export let datePickerSelectionColor = red;

  export let iconDefaultColor = warmGrey4;
  export let iconSuccessColor = success;

  export let loaderColor = red;
};

export namespace Spacing {
  export const verticalBetweenFields = 30;
  export const horizontalBetweenFields = 20;
  export const betweenPageSections = 24;
}

export namespace Breakpoints {
  /** Phone */
  export const sm = 480;
  /** Tablet */
  export const lg = 970;
}

export namespace MaxWidths {
  /**
   * You normally don't want to provide a too wide UI even if the user has a big wide screen
   * So we limit the screens to this width based on the prototypes
   */
  export const pageContent = 1080;
  /**
   * For form pages we want the content limited to this width.
   * NOTE: this number came from auspost sketch files width
   */
  export const formContent = 610;
  /**
   * For simple dialog pages e.g. Login
   */
  export const dialogContent = 350;
}

export namespace FontSizes {
  export const h1 = '36px';
  export const h2 = '30px';
  export const h3 = '24px';
  export const h4 = '18px';
  export const h5 = '15px';

  export const lineHeight = 1.25;

  export const paragraph = '15px';
  export const paragraphLineHeight = '24px';

  export const label = '15px';
  export const input = '1rem';
  export const buttonText = '15px';

  /** For legal text / field errors etc. */
  export const smallText = '12px';

  export let alertBarText = '14px';
}

/**
 * Managing zIndexes
 */
export const zIndexes = {
  tooltip: 100,
  headerMenuItemsRoot: 10,
  headerMenuPopdownButton: 12,
}

export namespace FontFamilies {
  /** Used for most headings / headers etc. */
  export const defaultFont = "Arial, Lato, 'Helvetica Neue', Helvetica, sans-serif";
  /** Used for labels and some copy text etc */
  export const arial = 'ArialMT, Arial, sans-serif';

  /** Semantic names for fonts */
  export let buttons = defaultFont;
  export let headings = defaultFont;

  export let labels = arial;
  export let text = arial;
  export let alerts = arial;

  export let alertBar = defaultFont;
}

export namespace DefaultStyles {
  export let borderRadius = 3;
}