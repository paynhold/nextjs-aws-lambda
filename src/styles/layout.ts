import { compose, css } from 'glamor';

export const content = css({
  MsFlexNegative: 0,
  WebkitFlexShrink: 0,
  flexShrink: 0,
  flexBasis: 'auto',
});

export const flex = css({
  display: ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex']
});
export const flexColumn = css({
  MsFlexDirection: 'column',
  WebkitFlexDirection: 'column',
  flexDirection: 'column',
});
export const flexRow = css({
  MsFlexDirection: 'row',
  WebkitFlexDirection: 'row',
  flexDirection: 'row',
});

// Alignment in cross axis
export const flexStart = css({
  MsFlexAlign: 'start',
  WebkitAlignItems: 'flex-start',
  alignItems: 'flex-start',
});
export const flexCenter = css({
  MsFlexAlign: 'center',
  WebkitAlignItems: 'center',
  alignItems: 'center',
});
export const flexEnd = css({
  MsFlexAlign: 'end',
  WebkitAlignItems: 'flex-end',
  alignItems: 'flex-end',
});

// Alignment in main axis
export const flexStartJustified = css({
  MsFlexPack: 'start',
  WebkitJustifyContent: 'flex-start',
  justifyContent: 'flex-start'
});
export const flexCenterJustified = css({
  MsFlexPack: 'center',
  WebkitJustifyContent: 'center',
  justifyContent: 'center'
});
export const flexEndJustified = css({
  MsFlexPack: 'end',
  WebkitJustifyContent: 'flex-end',
  justifyContent: 'flex-end'
});
export const flexAroundJustified = css({
  MsFlexPack: 'distribute',
  WebkitJustifyContent: 'space-around',
  justifyContent: 'space-around'
});
export const flexBetweenJustified = css({
  MsFlexPack: 'justify',
  WebkitJustifyContent: 'space-between',
  justifyContent: 'space-between'
});

// Alignment in both axes //
export const flexCenterCenter = compose(flex, flexCenter, flexCenterJustified);

// Self alignment //
export var flexSelfStart = css({
  MsFlexItemAlign: 'start',
  WebkitAlignSelf: 'flex-start',
  alignSelf: 'flex-start'
});
export const flexSelfCenter = css({
  MsFlexItemAlign: 'center',
  WebkitAlignSelf: 'center',
  alignSelf: 'center'
});
export const flexSelfEnd = css({
  MsFlexIitemAlign: 'end',
  WebkitAlignSelf: 'flex-end',
  alignSelf: 'flex-end'
});
export const flexSelfStretch = css({
  MsFlexItemAlign: 'stretch',
  WebkitAlignSelf: 'stretch',
  alignSelf: 'stretch',
});

export const flexWrap = css({
  MsFlexWrap: 'wrap',
  WebkitFlexWrap: 'wrap',
  flexWrap: 'wrap',
});

export const flexNoWrap = css({
  MsFlexWrap: 'nowrap',
  WebkitFlexWrap: 'nowrap',
  flexWrap: 'nowrap',
});

export const fullScreen = css({
  minHeight: '100vh',
  minWidth: '100%',
});

export const fillFlexParent = css({
  WebkitFlex: '1 auto',
  MsFlex: '1 auto',
  flex: '1 auto',
});