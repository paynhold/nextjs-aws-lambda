import { css } from 'glamor';

export const resetUl = css({
  listStyle: 'none',
  padding: 0,
  margin: 0
});